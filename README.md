# 4D flow tensor MRI synthesis for turbulent flow with bayesian unfolding

**A demo code for generation of synthetic multipoint 4D flow MRI** 

This code supplements the article *Synthesis of patient-specific multipoint 4D flow MRI data of turbulent aortic flow downstream of stenotic valves* (Dirix et al., Sci Rep 12, 16004 (2022) https://doi.org/10.1038/s41598-022-20121-x) and contains the necessary data and scripts to reproduce Figures 3b, 3c and 3d.  

**Hardware** 

Testing was performed on a Ubuntu 18.04 LTS using the Windows Subsystem for Linux. Bayesian unfolding of the synthetic data is highly parallelizable and was tested with up to 48 processing cores using just-in-time (JIT) compilation of relevant Python snippets. GPU acceleration is not implemented.  

---
## Table of Contents
1. [Getting Started](#1-getting-started)    
    1.1 [Installation](#11-installation)     
    1.2 [Files](#12-files)
2. [Running the synthesizer](#2-running-the-synthesizer)    
    2.1 [Steps to follow](#21-steps-to-follow)    
    2.2 [Bayesian unfolding](#22-bayesian-unfolding)    
    2.3 [Visualization](#23-visualization)        

## 1. Getting Started

### 1.1 Installation
Pull the repository to a local directory. A [conda .yml environment](4DFlowSynthesisEnv.yml) is available for automatic generation of an anaconda python environment. Alternatively, the [required packages](requirements) can be installed manually. 

### 1.2 Files
This demo is based on a computational fuid dynamics (CFD) simulation of pulsatile flow in an [idealized eccentric stenotic tube](CFD/pv_computed_averaged_20c_P11.vtk) during post systolic peak turbulence. A large eddy simulation (LES) is performed with a wall-adapting local eddy viscosity (WALE) model for turbulence. The mean Reynolds numbers are 1000 (4000 at peak systole) and 2000 at the inlet and the stenotic throat, respectively. The stenotic throat consists in a 75% area reduction with a 5% eccentricity.

The [results](Results) of the synthetic MRI pipeline are also uploaded to the repository. The synthetic MRI images are labeled as *mri_snrXXX_voxelXXX_dtXXX_P11.vti*. The files contain data on *Vcart* (velocity in $`ms^{-1}`$) and *RST_recon* (Reynolds stress tensor in $`Jm^{-3}`$). Additionally, the files *data_XXX.npy* containing the values of integrated total turbulence kinetic energy ($`tTKE \: [mJ]`$) are also present alongside the corresponding comparative *FigureXXX.png*.

## 2. Running the synthesizer 

### 2.1 Steps to follow  
Run [*Synthesizer.py*](Synthesizer.py). The parametric study domain is defined by default to compute 16 synthetic cases based on 4 different MRI voxel sizes (1,1.5,2 and 2.5$`mm`$) and 4 different temporal averagings (0,5,10 and 20$`ms`$):
```python 
#Definition of the parametric grid 4 voxel sizes x 4 time averagings
voxel_map=[1,1.5,2,2.5] #Voxel size [mm]
dtavg_map=[0,1,2,3] #0=0ms, 1=5ms, 2=10ms, 3=20ms

frames=[]
for f in files:
    for voxel in voxel_map[:]:
        for dtavg in dtavg_map[:]:
            frames.append([f,voxel,dtavg])

mri_synthesis(frames)
```
The MRI voxel sizes are arbitrary, however the temporal averagings depend on the pre-computed CFD data and only includes data for instantaneous shots (0$`ms`$) and averaged solutions with temporal windows of 5,10 and 20$`ms`$. After the completion of the data generation, run [*gen_data_cfd.py*](Plotter/gen_data_cfd.py) and [*gen_data_mri.py*](Plotter/gen_data_mri.py) to compute $`tTKE=\iiint_V TKE \,dx\,dy\,dz`$) for all the cases. Finally, run [*2Dplotter.py*](Plotter/2Dplotter.py) to generate the comparison plots show in Figures 3c and 3d of the manuscript. 

### 2.2 Bayesian unfolding
The bayesian unfolding runs in serial per default. If multiple processing cores are available, it is possible to specify them when launching the bayesian unfolding by modifying the *n_proc* flag in [*Synthesizer.py*](Synthesizer.py):
```python 
#Bayesian unfolding of multiple encoding strengths
v,s=Bayes().fragmentBayes(probed_image['rS'][np.where(def_mask_cfd>0.5)]+1j*probed_image['iS'][np.where(def_mask_cfd>0.5)], kv_rec,n_proc=1)
```

### 2.3 Visualization
Both unstructured (.vtk) and image files (.vti) can be visualized using Paraview to recreate Figure 3b of the manuscript, Figures 3c and 3d are obtained by running the [*2Dplotter.py*](Plotter/2Dplotter.py) script.
