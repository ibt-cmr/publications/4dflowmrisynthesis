"""
Signal based 4D flow MRI synthesizer
Class containing filtering masks for fourier space apodization.

Copyright © 2022 Pietro Dirix, Stefano Buoso, Eva Peper and Sebastian Kozerke
Licensed under the MIT License (see LICENSE for details)
Date: 06-2022
Author: Pietro Dirix
"""

import numpy as np

class filters:
    """The base class for fourier space filters.
    """
    def distance1D(self,point1,point2):
        """Computes the distance between two points
        :param point1: first point (2d or 3d)
        :param point2: second point (2d or 3d)
        :return the distance between points 1 and 2
        """
        return np.abs(point1-point2)

    def butterworth3D_carth(self,rf,imgShape,n=200):
        """Generates a Butterworth 3D filter window
        :param rf: reduction factor (3x1)
        :param imgShape: shape of the image (3x1)
        :param n: cut-off strength (1x1)
        :return 3D Butterworth filtering window
        """
        rows, cols, depth = imgShape[:3]
        Rows= np.linspace(0,rows-1,rows)
        Cols= np.linspace(0,cols-1,cols)
        Depth= np.linspace(0,depth-1,depth)
        R,C,D=np.meshgrid(Cols,Rows,Depth)
        center = (rows/2,cols/2,depth/2)
        base = 1/(1+(self.distance1D(C,center[0])/(imgShape[0]/rf[0]/2))**(2*n))\
                    *1/(1+(self.distance1D(R,center[1])/(imgShape[1]/rf[1]/2))**(2*n))\
                    *1/(1+(self.distance1D(D,center[2])/(imgShape[2]/rf[2]/2))**(2*n))

        return base

    def tukey3D_carth(self,rf,imgShape,alpha=0.5):
        """Generates a Tukey 3D filter window
        :param rf: reduction factor (3x1)
        :param imgShape: shape of the image (3x1)
        :param alpha: cut-off strength (1x1)
        :return 3D Tukey filtering window
        """
        base = np.zeros(imgShape[:3])
        tkx=base.copy()
        tky=base.copy()
        tkz=base.copy()
        rows, cols, depth = imgShape[:3]
        Rows= np.linspace(0,rows-1,rows)
        Cols= np.linspace(0,cols-1,cols)
        Depth= np.linspace(0,depth-1,depth)
        R,C,D=np.meshgrid(Cols,Rows,Depth)

        center = (rows/2,cols/2,depth/2)

        dx=self.distance1D(C,center[0])
        x_lim=np.where(dx<imgShape[0]/rf[0]/2*alpha)
        tkx[x_lim]=1
        x_lim=np.where((imgShape[0]/rf[0]/2*alpha<=dx) & (dx<=imgShape[0]/rf[0]/2))
        tkx[x_lim]=0.5*(1+np.cos((np.pi*(dx[x_lim]-alpha*imgShape[0]/rf[0]/2))/((1-alpha)*imgShape[0]/rf[0]/2)))

        dy=self.distance1D(R,center[1])
        y_lim=np.where(dy<imgShape[1]/rf[1]/2*alpha)
        tky[y_lim]=1
        y_lim=np.where((imgShape[1]/rf[1]/2*alpha<=dy) & (dy<=imgShape[1]/rf[1]/2))
        tky[y_lim]=0.5*(1+np.cos((np.pi*(dy[y_lim]-alpha*imgShape[1]/rf[1]/2))/((1-alpha)*imgShape[1]/rf[1]/2)))

        dz=self.distance1D(D,center[2])
        z_lim=np.where(dz<imgShape[2]/rf[2]/2*alpha)
        tkz[z_lim]=1
        z_lim=np.where((imgShape[2]/rf[2]/2*alpha<=dz) & (dz<=imgShape[2]/rf[2]/2))
        tkz[z_lim]=0.5*(1+np.cos((np.pi*(dz[z_lim]-alpha*imgShape[2]/rf[2]/2))/((1-alpha)*imgShape[2]/rf[2]/2)))

        base = tkx*tky*tkz

        return base

    def paddedTukeyWindow3D(self,N, red, alpha):
        """Generates a Tukey 3D filter window
        :param N: size
        :param red: reduction factor
        :param alpha: cut-off strength
        :return 3D Tukey filtering window
        """

        lw0 = int(np.ceil(N[0] / red))
        lw1 = int(np.ceil(N[1] / red))
        lw2 = int(np.ceil(N[2] / red))

        w0 = np.reshape(self.paddedTukeyWindow(N[0], lw0, alpha), (N[0], 1, 1))
        w1 = np.reshape(self.paddedTukeyWindow(N[1], lw1, alpha), (1, N[1], 1))
        w2 = np.reshape(self.paddedTukeyWindow(N[2], lw2, alpha), (1, 1, N[2]))


        return w0 * w1 * w2

    def TukeyWindow(self,length, alpha):
        """Generates a Tukey 1D filter window
        :param length: length
        :param alpha: cut-off strength
        :return 1D Tukey filtering window
        """
        t = np.linspace(0, 1, length)

        len_lo = int(np.floor( (alpha/2) * (length - 1)) + 1);
        len_hi = int(length - len_lo + 1);

        low = (1 + np.cos(2*np.pi / alpha * (t[0:len_lo] - alpha/2))) / 2
        const = np.ones((len_hi - len_lo - 1))
        high = (1 + np.cos(2*np.pi / alpha * (t[len_hi - 1:] - 1 + alpha/2))) / 2

        w = np.concatenate((low, const, high), axis=0)

        return w

    def paddedTukeyWindow(self,N, Nw, alpha=0.5):
        """Generates a Tukey 1D padded filter window
        :param N: padded size
        :param N: size
        :param alpha: cut-off strength
        :return 1D Tukey filtering window
        """
        if(alpha == 0):
            w1 = np.ones(Nw)
        else:
            w1 = self.TukeyWindow(Nw,alpha)
        l0 = int((N-Nw)/2)
        if( (N-Nw)%2 !=0):
            w0 = np.zeros(l0+1)
        else:
            w0 = np.zeros(l0)
        w2 = np.zeros(l0)
        w = np.concatenate((w0,w1,w2),axis=0)
        return w

    def gauss3D_carth(self,rf,imgShape):
        """Generates a Gaussian 3D filter window
        :param rf: gaussian reduction factor (3x1)
        :param imgShape: shape of the image (3x1)
        :return 3D Gaussian filtering window
        """
        base = np.zeros(imgShape[:3])
        gx=base.copy()
        gy=base.copy()
        gz=base.copy()
        rows, cols, depth = imgShape[:3]
        Rows= np.linspace(0,rows-1,rows)
        Cols= np.linspace(0,cols-1,cols)
        Depth= np.linspace(0,depth-1,depth)
        R,C,D=np.meshgrid(Cols,Rows,Depth)

        center = (rows/2,cols/2,depth/2)

        gx=np.exp(-(np.pi*self.distance1D(C,center[0])/self.distance1D(C,center[0]).max())**2/2/(rf[0])**2)
        gy=np.exp(-(np.pi*self.distance1D(R,center[1])/self.distance1D(R,center[1]).max())**2/2/(rf[1])**2)
        gz=np.exp(-(np.pi*self.distance1D(D,center[2])/self.distance1D(D,center[2]).max())**2/2/(rf[2])**2)

        base = gx*gy*gz

        return base
