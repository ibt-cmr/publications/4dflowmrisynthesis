"""
Signal based 4D flow MRI synthesizer
Main script.

Copyright © 2022 Pietro Dirix, Stefano Buoso, Eva Peper and Sebastian Kozerke
Licensed under the MIT License (see LICENSE for details)
Date: 06-2022
Author: Pietro Dirix
"""
#Standard libraries
import numpy as np
import os,sys, glob
import pyvista as pv
#User defined libraries
from filters import filters
from functionsRST import functionsRST
from functionsRST import Bayes
from functionsRST import processData

def mri_synthesis(input_param):
    """Generates a synthetic 4D flow MRI dataset
    :param list input_param: set of parameters for the simulation [path, voxel size, time averaging, mode]
    :return the synthetic 4D flow MRI dataset
    :rtype Image Data (VTI)
    """
    for param in input_param:
        frames=[param[0]]
        if param[2]==0:
            snr=1000
        else:
            snr=param[1]**3*np.sqrt([0,5,10,20][param[2]])*1.68 #SNR = alpha*Volume*sqrt(dT)

        #MRI voxel size (resolution)
        voxel_sizex =param[1]/1000
        voxel_sizey =param[1]/1000
        voxel_sizez =param[1]/1000

        target_RST='RST'+str(param[2])
        target_U='U'+str(param[2])

        #Image voxel size (underlying grid)
        image_sizex=6.5e-4
        image_sizey=6.5e-4
        image_sizez=6.5e-4

        #Velocity encodings
        venc = np.array([0.5,1.5,3])
        enc_dirs = np.array([[1., 0., 0.],
                 [0.,1.,0.],
                 [0.,0.,1.],
                 [np.sqrt(2)/2,np.sqrt(2)/2,0.],
                 [np.sqrt(2)/2,0.,np.sqrt(2)/2],
                 [0.,np.sqrt(2)/2,np.sqrt(2)/2]])
        kv_rec= np.concatenate(([0],np.pi/venc))
        kv_rec = kv_rec[:,None]
        kv_rec = np.tile(kv_rec, (1, len(enc_dirs)))

        n_frames = len(frames)
        mesh = pv.read(frames[0])
        Coords_0  = mesh.points

        #Generation of background uniform grid
        #Image bounds were cropped to a region of interest for this demo
        image_box = [(np.max(Coords_0[:,0])-np.min(Coords_0[:,0]))*1.25/2,\
                (np.max(Coords_0[:,1])-np.min(Coords_0[:,1]))*1.4,\
                (np.max(Coords_0[:,2])-np.min(Coords_0[:,2]))*1.4]

        orig_image =[np.min(Coords_0,axis=0)[0],\
                np.min(Coords_0,axis=0)[1]-image_box[1]*1/3.5/2,\
                np.min(Coords_0,axis=0)[2]-image_box[2]*1/3.5/2]

        nx = int(image_box[0]/image_sizex)
        ny = int(image_box[1]/image_sizey)
        nz = int(image_box[2]/image_sizez)

        res_x = image_box[0]/nx
        res_y = image_box[1]/ny
        res_z = image_box[2]/nz

        resfctx    = voxel_sizex/res_x
        resfcty    = voxel_sizey/res_y
        resfctz    = voxel_sizez/res_z

        dims=(nx, ny, nz)
        spacing=(res_x,res_y,res_z)
        origin=(orig_image[0]+res_x/2,orig_image[1]+res_y/2,orig_image[2]+res_z/2)
        ref_image = pv.UniformGrid(dims,spacing,origin)

        #Filtering window (cropped gaussian)
        rf=(np.sqrt(8*np.log(2))/resfctx,np.sqrt(8*np.log(2))/resfcty,np.sqrt(8*np.log(2))/resfctz)
        wg=filters().gauss3D_carth(rf,dims)        
        wt=filters().paddedTukeyWindow3D(dims, max(1,resfctx), 0.01)
        w=wg*wt
        w=w/w.max()

        for counter, frame_sel in enumerate(frames):

            print('Generating 4D PC-MRI for frame ',counter,' out of ',n_frames,'(',frame_sel,')')

            mesh = pv.read( frame_sel)
            mesh=mesh.cell_data_to_point_data()

            probed_image=ref_image.sample(mesh)

            def_mask_cfd = probed_image.get_array('vtkValidPointMask')
            velocity = probed_image.get_array(target_U)
            RST_cfd = probed_image.get_array(target_RST)

            velocity_ds=velocity.copy()
            RST_ds=RST_cfd.copy()

            #Downsampling to MRI resolution of the velocity map
            for i in range(velocity_ds.shape[1]):
                velocity_ds[:,i]=np.real(functionsRST.downsample3D(nx,ny,nz,w,velocity_ds[:,i])[:,0])

            # Downsampling to MRI resolution of the Reynolds stress tensor (RST) map
            for i in range(RST_ds.shape[1]):
                if i ==0:
                    RST_ds[:,i]=np.real(functionsRST.downsample3D(nx,ny,nz,w,RST_ds[:,i])[:,0])+\
                                np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,0])**2))[:,0])+\
                                (velocity_ds[:,0])*np.real(functionsRST.downsample3D(nx,ny,nz,w,(-2*(velocity[:,0])))[:,0])+\
                                (velocity_ds[:,0])**2
                if i ==1:
                    RST_ds[:,i]=np.real(functionsRST.downsample3D(nx,ny,nz,w,RST_ds[:,i])[:,0])+\
                                np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,0])*(velocity[:,1])))[:,0])-\
                                velocity_ds[:,0]*np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,1])))[:,0])-\
                                velocity_ds[:,1]*np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,0])))[:,0])+\
                                velocity_ds[:,1]*velocity_ds[:,0]
                if i ==2:
                    RST_ds[:,i]=np.real(functionsRST.downsample3D(nx,ny,nz,w,RST_ds[:,i])[:,0])+\
                                np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,0])*(velocity[:,2])))[:,0])-\
                                velocity_ds[:,0]*np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,2])))[:,0])-\
                                velocity_ds[:,2]*np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,0])))[:,0])+\
                                velocity_ds[:,2]*velocity_ds[:,0]
                if i ==3:
                    RST_ds[:,i]=np.real(functionsRST.downsample3D(nx,ny,nz,w,RST_ds[:,i])[:,0])+\
                                np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,1])**2))[:,0])+\
                                (velocity_ds[:,1])*np.real(functionsRST.downsample3D(nx,ny,nz,w,(-2*(velocity[:,1])))[:,0])+\
                                (velocity_ds[:,1])**2
                if i ==4:
                    RST_ds[:,i]=np.real(functionsRST.downsample3D(nx,ny,nz,w,RST_ds[:,i])[:,0])+\
                                np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,1])*(velocity[:,2])))[:,0])-\
                                velocity_ds[:,1]*np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,2])))[:,0])-\
                                velocity_ds[:,2]*np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,1])))[:,0])+\
                                velocity_ds[:,2]*velocity_ds[:,1]
                if i ==5:
                    RST_ds[:,i]=np.real(functionsRST.downsample3D(nx,ny,nz,w,RST_ds[:,i])[:,0])+\
                                np.real(functionsRST.downsample3D(nx,ny,nz,w,((velocity[:,2])**2))[:,0])+\
                                (velocity_ds[:,2])*np.real(functionsRST.downsample3D(nx,ny,nz,w,(-2*(velocity[:,2])))[:,0])+\
                                (velocity_ds[:,2])**2

            def_mask_ds=((np.linalg.norm(velocity_ds,axis=1)/np.max(np.linalg.norm(velocity_ds,axis=1)))**(1/3)/2+0.5)

            probed_image['velocity_ds']=velocity_ds
            probed_image['rS']=np.zeros((probed_image.get_array(target_U).shape[0],len(enc_dirs)*len(venc)+1))
            probed_image['iS']=np.zeros((probed_image.get_array(target_U).shape[0],len(enc_dirs)*len(venc)+1))

            #Generation of the reference shot (no velocity encoding)
            S0 = def_mask_ds+0j
            noise_std=np.mean(np.abs(S0[np.where(def_mask_cfd>0.5)]))/snr
            noise_S0     = (np.random.normal(0,noise_std,(nx,ny,nz)) \
                + 1j*np.random.normal(0,noise_std,(nx,ny,nz)))
            noise_S0 =  noise_S0*np.fft.fftshift(w)
            noise_img_S0 = np.fft.ifftn(noise_S0)
            noise_img_S0 = noise_img_S0*noise_std/np.std(np.real(noise_img_S0))

            S0= S0[:,np.newaxis]+noise_img_S0.reshape(-1,1,order="F")

            probed_image['rS'][:,0]=np.real(S0[:,0])
            probed_image['iS'][:,0]=np.imag(S0[:,0])

            #Generation of each image for each encoding direction and strength
            for direction,enc_dir in enumerate(enc_dirs):
                for nkv, kv in enumerate(kv_rec[1:,direction]):
                    print('Synthesizing dir ',direction,' with kv=',kv)

                    phase_dir = np.exp(1j*kv*velocity_ds.dot(enc_dir))
                    var=functionsRST.kvTRSTkv(RST_ds,enc_dir*kv)
                    signal_loss = np.exp(-var/2)
                    Skv= phase_dir*signal_loss*def_mask_ds

                    noise_std=np.mean(np.abs(S0[np.where(def_mask_cfd>0.5)]))/snr
                    noise     = (np.random.normal(0,noise_std,(nx,ny,nz)) \
                        + 1j*np.random.normal(0,noise_std,(nx,ny,nz)))
                    noise =  noise*np.fft.fftshift(w)
                    noise_img = np.fft.ifftn(noise)
                    noise_img = noise_img*noise_std/np.std(np.real(noise_img))

                    Skv += noise_img.reshape(-1,1,order="F")[:,0]

                    probed_image['rS'][:,direction*len(venc)+nkv+1]=np.real(Skv)
                    probed_image['iS'][:,direction*len(venc)+nkv+1]=np.imag(Skv)

        #Bayesian unfolding of multiple encoding strengths
        v,s=Bayes().fragmentBayes(probed_image['rS'][np.where(def_mask_cfd>0.5)]+1j*probed_image['iS'][np.where(def_mask_cfd>0.5)], kv_rec,n_proc=1)

        V=np.zeros((probed_image['rS'].shape[0],6))
        S=V.copy()
        V[np.where(def_mask_cfd>0.5)]=v
        S[np.where(def_mask_cfd>0.5)]=s
        probed_image['Vcart']=processData.getCartVelocities(V,enc_dirs)
        probed_image['RST_recon']=processData.calcRST_ivsd(S,enc_dirs)
        probed_image=probed_image.point_data_to_cell_data()

        #Generate output grid
        cleaned_image=pv.UniformGrid()
        cleaned_image.copy_structure(probed_image)
        cleaned_image['Vcart']=probed_image['Vcart']
        cleaned_image['RST_recon']=probed_image['RST_recon']
        cleaned_image['vtkValidPointMask']=probed_image['vtkValidPointMask']
        cleaned_image.save('mri_snr'+str(snr)+'_voxel'+str(param[1]).replace('.','')+'_dt'+str(param[2])+'_.vti')

def chunks(input, n):
    """Yields successive n-sized chunks of input"""
    for i in range(0, len(input), n):
        yield input[i:i + n]

files=glob.glob(os.path.join(os.getcwd(),'CFD/*.vtk'))

#Definition of the parametric grid 4 voxel sizes x 4 time averagings
voxel_map=[1,1.5,2,2.5]
dtavg_map=[0,1,2,3]

frames=[]
for f in files:
    for voxel in voxel_map[:]:
        for dtavg in dtavg_map[:]:
            frames.append([f,voxel,dtavg])

mri_synthesis(frames)