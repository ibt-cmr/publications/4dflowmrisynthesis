"""
Signal based 4D flow MRI synthesizer
Class containing Reynolds stress tensor based functionalities.

Copyright © 2022 Pietro Dirix, Stefano Buoso, Eva Peper and Sebastian Kozerke
Licensed under the MIT License (see LICENSE for details)
Date: 06-2022
Author: Pietro Dirix based on previous work by Jonas Walheim
"""

import numpy as np
import scipy.optimize as spoptimize
from numba import jit
import multiprocessing

class functionsRST:
    """The base class for fourier space downsampling and RST multiplication.
    """
    def downsample3D(nx,ny,nz,w,U):
        """Spatial downsampling by apodization in fourier space
        :params nx,ny,nz: image size
        :param w: apodization window (nx x ny x nz)
        :param U: target field (nx x ny x nz)
        :return downsampled target
        """
        Uds=U.copy()
        Uds=np.fft.fftn(Uds.reshape(nx,ny,nz,order="F"))
        Uds= np.fft.ifftshift(np.fft.fftshift(Uds)*w)
        Uds = np.fft.ifftn(Uds)

        return Uds.reshape(-1,1,order="F")

    def kvTRSTkv(RST,kv):
        """Operator for efficient computing of kv.T.dot(RST.dot(kv)).
        :params RST: Reynolds stress tensor
        :param kv: encoding vector
        :return solution to kv.T.dot(RST.dot(kv))
        """
        var=RST[:,0]*kv[0]**2+RST[:,1]*kv[0]*kv[1]+RST[:,2]*kv[0]*kv[2]+\
            RST[:,1]*kv[0]*kv[1]+RST[:,3]*kv[1]**2+RST[:,4]*kv[1]*kv[2]+\
            RST[:,2]*kv[0]*kv[2]+RST[:,4]*kv[1]*kv[2]+RST[:,5]*kv[2]**2

        return var

class Bayes:
    """The base class for Bayesian unfolding.
    """
    def fragmentBayes(self,img,kv,n_proc):
        """Main script for parallel Bayesian unfolding.
        :param img: sequence of signals to combine
        :param kv: encoding matrix
        :param n_proc: number of processors
        :return results_v: unfolded velocity
        :return results_std: unfolded standard deviation
        """
        def chunks(input, n):
            """Yields successive n-sized chunks of input"""
            for i in range(0, len(input), n):
                yield input[i:i + n]

        sliced_img={}
        for n,i in enumerate(range(n_proc)):
            if n<n_proc-1:
                sliced_img[str(n)]=img[n*int(img.shape[0]/n_proc):(n+1)*int(img.shape[0]/n_proc)]
            else:
                sliced_img[str(n)]=img[n*int(img.shape[0]/n_proc):]

        manager = multiprocessing.Manager()
        return_dict_v = manager.dict()
        return_dict_std = manager.dict()
        processes=[]
        for i in np.arange(0,n_proc):
            #Execute the target function on the n_proc target processors using the splitted input
            p = multiprocessing.Process(target=self.BayesUnfold,args=(sliced_img[str(i)],kv,return_dict_v,return_dict_std,str(i)))
            processes.append(p)
            p.start()
        for process in processes:
            process.join()

        results_v=return_dict_v['0']
        results_std=return_dict_std['0']
        if n_proc>1:
            for i in np.arange(0,n_proc-1):
                results_v=np.concatenate((results_v,return_dict_v[str(i+1)]))
                results_std=np.concatenate((results_std,return_dict_std[str(i+1)])) 
        print(results_v,results_std)
        
        return results_v,results_std

    def BayesUnfold(self,img, kv, return_dict_v=None,return_dict_std=None,proc_n=None):
        """Bayesian unfolding.
        --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
        :param img: image data; segment order: [ref,kv_x1,kv_y1,kv_z1,kv_x2,kv_y2,kv_z2]
        :param kv: encoding matrix [kv_x1,0,0; 0,kv_y1,0; ...]
        :return results_v: unfolded velocity
        :return results_std: unfolded standard deviation
        """
        noDirs = np.size(kv, 1)
        print('start bayes unfolding, input image size: ', img.shape,' \t number of directions: ',noDirs)

        results_v = np.zeros((img.shape[0], noDirs))
        results_std = results_v.copy()

        img = img * np.conj(img[:,0])[:,np.newaxis]

        for indDir in range(noDirs):
            kv_full = kv.copy()

            kv_curr = kv[:, indDir]

            no_pts = np.sum(kv_curr != 0)
            if(no_pts ==1):
                ind_data = np.array([0, 1 + indDir ])
            elif(no_pts ==2):
                ind_data = np.array([0,1+indDir*no_pts, 2+indDir*no_pts])
            elif(no_pts==3):
                ind_data = np.array([0, 1 + indDir*no_pts, 1 + indDir*no_pts+1, 1+indDir*no_pts+2]) 
            else:
                print('multipoint unfolding only works with 2 or 3 points for now')
            print('ind_data', ind_data)

            print('Solving for direction ', indDir,' with kv: ',kv_curr)
            venc = np.pi / np.min(np.abs(kv_curr[kv_curr != 0]))

            sdata=img[:,ind_data]
            for i in range(sdata.shape[0]):
                if i%10000==1:
                    print('Solving... node: ',i)
                results_v[i,indDir], results_std[i,indDir] = self.solve_TKE(sdata[i], kv_curr)

        return_dict_v[proc_n]=results_v
        return_dict_std[proc_n]=results_std

    def solve_TKE(self,sdata, kv, verbose=0):
        """Minimizing probability using the Nelder-Mead algorithm.
        --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
        :param sdata: image data
        :param kv: encoding vector
        :return v_out: unfolded velocity
        :return D_out: unfolded standard deviation
        """
        VSTEP_SCALE = 30 # define grid size for initial v estimate

        Nkv = np.size(sdata,0)

        D_est = estimate_sigma(sdata, kv, Nkv)

        # Estimate vel
        venc = np.pi / np.min(kv[kv > 0]);  # Venc is from the smallest non-zero kv point
        vstep = np.pi / np.max(kv[kv > 0]) / VSTEP_SCALE;  # // Step size is in reference to the smallest VENC
        v_est = estimate_vel(venc, vstep, D_est, sdata, kv, Nkv);

        # estimate v_est, D_est for Nelder-Mead search
        x0 = np.array([v_est, D_est])

        if np.isnan(D_est):
            D_est = 0.

        # define probfun as function of x (= v, sigma) with
        probfun = lambda x: postprob(x, sdata, kv, Nkv)

        # search maximum probability (-log..), with starting point x0
        # output: v_out, D_out
        out = spoptimize.minimize(probfun, x0, method='Nelder-Mead', options={'disp': False, 'maxiter':50})
        #out = spoptimize.minimize(probfun, x0, method='BFGS')

        v_out = out.x[0]
        D_out = out.x[1]

        return v_out, D_out


@jit(nopython = True)
def postprob(x_in, sdata, kv, Ns):
    """Computation of posterior probability.
    --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
    :param x_in: lambda variable (for function definition)
    :param sdata: image data
    :param kv: encoding vector
    :return compprob: probability map
    """
    sdata_r = np.real(sdata)
    sdata_i = np.imag(sdata)

    v = x_in[0]
    sig = x_in[1]

    m = 2
    sigsq = sig * sig

    eig1 = 0.0;
    for i in range(Ns):
        eig1 = eig1 + np.exp(-sigsq * kv[i] * kv[i])
    eig1 = 1 / eig1;

    h1 = 0.0;
    h2 = 0.0;
    for i in range(Ns):
        h1 = h1 + np.exp(-0.5 * sigsq * kv[i] * kv[i]) * (
        sdata_r[i] * np.sin(v * kv[i]) + sdata_i[i] * np.cos(v * kv[i]));
        h2 = h2 + np.exp(-0.5 * sigsq * kv[i] * kv[i]) * (
        sdata_r[i] * np.cos(v * kv[i]) - sdata_i[i] * np.sin(v * kv[i]));

    h1 = h1 * np.sqrt(eig1)
    h2 = h2 * np.sqrt(eig1)

    mh2 = h1 * h1 + h2 * h2

    denom = 0.0
    for i in range(Ns):
        denom = denom + sdata_r[i] * sdata_r[i] + sdata_i[i] * sdata_i[i];

    if(denom==0.0):
        denom = 1e-10

    compprob = -np.log(eig1 * (1 - mh2 / denom) ** ((m - 2 * Ns) / 2));

    return compprob


@jit(nopython = True)
def estimate_vel(venc, vstep, D_est, sdata, kv, Nkv):
    """Estimation of velocity.
    --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
    :param venc: venc from smallest non zero kv point
    :param vstep: step size
    :param D_est: estimated standard deviation
    :param sdata: image data
    :param kv: encoding vector
    :param Nkv: image data size
    :return v_est: estimated velocity
    """
    xin = [-venc, D_est];

    min_tprob = postprob(xin, sdata, kv, Nkv)
    v_est = -venc;
    tprob = 0.0;

    for v in np.arange(-venc, venc, vstep):
        xin[0] = v;
        tprob = postprob(xin, sdata, kv, Nkv)
        if (tprob < min_tprob):
            min_tprob = tprob
            v_est = v

    return v_est


@jit(nopython = True)
def estimate_sigma(sdata, kv, Nkv):
    """Estimation of standard deviation.
    --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
    :param sdata: image data
    :param kv: encoding vector
    :param Nkv: image data size
    :return sig: estimated standard deviation
    """
    norm = 0.0
    for i in range(Nkv):
        norm = norm + (0.5 * kv[i] * kv[i]) ** 2

    sig = 0.0;
    y0 = np.abs(sdata[0])

    y = 0.0;
    for i in range(Nkv):
        y = np.log(y0 / (abs(sdata[i]) + 1e-16 ))
        if (y < 0):
            y = 0.0

        sig = 0.5 * kv[i] ** 2 * y / norm + sig

    sig = np.sqrt(np.abs(sig))
    return  sig

class processData:
    """The base class for data reconstruction.
    """
    def getCartVelocities(v,kv):
        """Convert directional velocities to cartesian velocities.
        --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
        :param v: image data
        :param kv: encoding directions!
        :return v_cart: velocities along cartesian axes
        """
        A_dir = kv/np.sqrt(np.sum(kv**2,1))[:,None]
        Ainv = np.linalg.pinv(A_dir);
        v_cart = np.matmul(Ainv,v.T).T
        return v_cart


    def calcRST_ivsd(ivsd, kv, density=1060):
        """Determine Reynolds stress tensor from intra-voxel standard deviations.
        --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
        :param ivsd: intra voxel standard deviations
        :param kv: encoding directions!
        :param density: fluid density
        :return RST: Reynolds stress tensor
        """
        H = np.zeros((np.size(kv, 0), 6))

        for i in range(np.size(kv, 0)):
            H[i, :] = [kv[i, 0] ** 2, kv[i, 1] ** 2, kv[i, 2] ** 2,
                       2 * kv[i, 0] * kv[i, 1], 2 * kv[i, 0] * kv[i, 2], 2 * kv[i, 1] * kv[i, 2]]

        kv_magsq = np.reshape(np.sum(kv ** 2, 1), (6, 1))
        Hdir = H / kv_magsq;
        Hinv = np.linalg.pinv(Hdir);

        var = ivsd.copy() ** 2
        RST = np.matmul(var, Hinv.T)*density

        return RST

    def calcPrincipalStresses(RST, density=1060):
        """Compute principal stresses.
        --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
        :param RST: Reynolds stress tensor
        :param density: fluid density
        :return ps: principal stresses
        """
        ps = np.zeros((np.size(RST, 0), 3))
        ps = calcPS(ps, RST.T)*density

        return ps

@jit()
def calcPS(ps, RST):
    """Compute principal stresses.
    --> Legacy code by Jonas Walheim, adapted by Pietro Dirix
    :param ps: principal stresses size
    :param RST: Reynolds stress tensor
    :return ps: principal stresses
    """

    for i in range(len(RST[0, :])):
        RST_curr = np.array([[RST[0, i], RST[3, i], RST[4, i]],
                             [RST[3, i], RST[1, i], RST[5, i]],
                             [RST[4, i], RST[5, i], RST[2, i]]])

        w = np.linalg.eigvalsh(RST_curr)
        ps[i, :] = w
    return ps
