"""
Signal based 4D flow MRI synthesizer
Generate comparative Figs 2c and 2d (see publication).

Copyright © 2022 Pietro Dirix, Stefano Buoso, Eva Peper and Sebastian Kozerke
Licensed under the MIT License (see LICENSE for details)
Date: 06-2022
Author: Pietro Dirix
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.ticker import LogFormatter 
plt.rcParams.update({'font.size': 22})

#Generate comparative plot for the whole domain
mri_data=np.load('data_mri.npy',allow_pickle=True).item()
cfd_data=np.load('data_cfd.npy',allow_pickle=True).item()

colormap='inferno'

x=np.array([0,1,2,3])
y=np.array([0,1,2,3])
z=np.zeros((4,4))
z[0,0]=(mri_data['tke_mri']['1_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[0,1]=(mri_data['tke_mri']['15_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[0,2]=(mri_data['tke_mri']['2_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[0,3]=(mri_data['tke_mri']['25_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100

z[1,0]=(mri_data['tke_mri']['1_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[1,1]=(mri_data['tke_mri']['15_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[1,2]=(mri_data['tke_mri']['2_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[1,3]=(mri_data['tke_mri']['25_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100

z[2,0]=(mri_data['tke_mri']['1_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[2,1]=(mri_data['tke_mri']['15_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[2,2]=(mri_data['tke_mri']['2_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[2,3]=(mri_data['tke_mri']['25_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100

z[3,0]=(mri_data['tke_mri']['1_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[3,1]=(mri_data['tke_mri']['15_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[3,2]=(mri_data['tke_mri']['2_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[3,3]=(mri_data['tke_mri']['25_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
print('TKE % ',z.T)

fig, axs = plt.subplots()
img=axs.pcolormesh(x, y, z.T, vmin=np.min(z), vmax=np.max(z), shading='auto')
axs.set_title("shading='auto' = 'nearest'")
plt.yticks((0,1,2,3),['1 $mm$','1.5$mm$','2$mm$','2.5$mm$'])
plt.xticks((0,1,2,3),['0$ms$','5$ms$','10$ms$','20$ms$'])
plt.xlabel("Time-avergaing [$ms$]")
plt.ylabel("Isotropic voxel size [$mm$]")
plt.title('Turbulence kinetic energy difference [%]')

pcm = axs.pcolor(x, y, z.T,
                   norm=colors.Normalize(vmin=0, vmax=100),
                   cmap=colormap, shading='auto')

formatter = LogFormatter(10, labelOnlyBase=False)
cb=fig.colorbar(pcm,ax=axs,label=r'Synthetic $TKE$ error [%] vs CFD')
ticks_l=np.array([0,10,20,30,40,50,60,70,80,90,100])
minorticks_l=(np.array([0,10,20,30,40,50,60,70,80,90,100]))
cb.set_ticks((ticks_l))
cb.set_ticklabels(ticks_l)
cb.ax.yaxis.set_ticks(minorticks_l, minor=True)

#Generate comparative plot for a region of interest
mri_data=np.load('data_mri_roi.npy',allow_pickle=True).item()
cfd_data=np.load('data_cfd.npy',allow_pickle=True).item()

colormap='inferno'

x=np.array([0,1,2,3])
y=np.array([0,1,2,3])
z=np.zeros((4,4))
z[0,0]=(mri_data['tke_mri']['1_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[0,1]=(mri_data['tke_mri']['15_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[0,2]=(mri_data['tke_mri']['2_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[0,3]=(mri_data['tke_mri']['25_0']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100

z[1,0]=(mri_data['tke_mri']['1_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[1,1]=(mri_data['tke_mri']['15_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[1,2]=(mri_data['tke_mri']['2_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[1,3]=(mri_data['tke_mri']['25_1']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100

z[2,0]=(mri_data['tke_mri']['1_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[2,1]=(mri_data['tke_mri']['15_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[2,2]=(mri_data['tke_mri']['2_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[2,3]=(mri_data['tke_mri']['25_2']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100

z[3,0]=(mri_data['tke_mri']['1_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[3,1]=(mri_data['tke_mri']['15_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[3,2]=(mri_data['tke_mri']['2_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
z[3,3]=(mri_data['tke_mri']['25_3']-cfd_data['tke_cfd0']['0'])/cfd_data['tke_cfd0']['0']*100
print('TKE % ',z.T)

fig, axs = plt.subplots()
img=axs.pcolormesh(x, y, z.T, vmin=np.min(z), vmax=np.max(z), shading='auto')
axs.set_title("shading='auto' = 'nearest'")
plt.yticks((0,1,2,3),['1 $mm$','1.5$mm$','2$mm$','2.5$mm$'])
plt.xticks((0,1,2,3),['0$ms$','5$ms$','10$ms$','20$ms$'])
plt.xlabel("Time-avergaing [$ms$]")
plt.ylabel("Isotropic voxel size [$mm$]")
plt.title('Turbulence kinetic energy difference [%]')

pcm = axs.pcolor(x, y, z.T,
                   norm=colors.Normalize(vmin=0, vmax=100),
                   cmap=colormap, shading='auto')

formatter = LogFormatter(10, labelOnlyBase=False)
cb=fig.colorbar(pcm,ax=axs,label=r'Synthetic $TKE$ error [%] vs CFD')
ticks_l=np.array([0,10,20,30,40,50,60,70,80,90,100])
minorticks_l=(np.array([0,10,20,30,40,50,60,70,80,90,100]))
cb.set_ticks((ticks_l))
cb.set_ticklabels(ticks_l)
cb.ax.yaxis.set_ticks(minorticks_l, minor=True)

plt.show()