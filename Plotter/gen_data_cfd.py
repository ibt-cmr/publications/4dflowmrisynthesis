"""
Signal based 4D flow MRI synthesizer
Extract turbulent kinetic energy data from CFD.

Copyright © 2022 Pietro Dirix, Stefano Buoso, Eva Peper and Sebastian Kozerke
Licensed under the MIT License (see LICENSE for details)
Date: 06-2022
Author: Pietro Dirix
"""

import glob,os
import numpy as np
import pyvista as pv

cfd_files=glob.glob(os.path.join(os.getcwd(),'../CFD/*.vtk'))

tke_cfd0={}
tke_cfd1={}
tke_cfd2={}
tke_cfd3={}

ke_cfd0={}
ke_cfd1={}
ke_cfd2={}
ke_cfd3={}

for e,cfd in enumerate(cfd_files):
    print('Extracting cfd data n',e)
    cfd_mesh=pv.read(cfd)
    cfd_mesh=cfd_mesh.clip('x',origin=[0.43092477226666376,0,0])
    vol_cfd=cfd_mesh.compute_cell_sizes(length=False, area=False, volume=True)['Volume']
    
    print('Time averaging 0ms')
    tke_cfd0[str(e)]=np.sum(np.sum(cfd_mesh['RST0'][:,[0,3,5]],axis=1)*vol_cfd)*1/2*1060*1000 #mJ
    ke_cfd0[str(e)]=np.sum(np.sum(cfd_mesh['U0']**2,axis=1)*vol_cfd)*1/2*1060*1000 #mJ
    print('Time averaging 5ms')
    tke_cfd1[str(e)]=np.sum(np.sum(cfd_mesh['RST1'][:,[0,3,5]],axis=1)*vol_cfd)*1/2*1060*1000 #mJ
    ke_cfd1[str(e)]=np.sum(np.sum(cfd_mesh['U1']**2,axis=1)*vol_cfd)*1/2*1060*1000 #mJ
    print('Time averaging 10ms')
    tke_cfd2[str(e)]=np.sum(np.sum(cfd_mesh['RST2'][:,[0,3,5]],axis=1)*vol_cfd)*1/2*1060*1000 #mJ
    ke_cfd2[str(e)]=np.sum(np.sum(cfd_mesh['U2']**2,axis=1)*vol_cfd)*1/2*1060*1000 #mJ
    print('Time averaging 20ms')
    tke_cfd3[str(e)]=np.sum(np.sum(cfd_mesh['RST3'][:,[0,3,5]],axis=1)*vol_cfd)*1/2*1060*1000 #mJ
    ke_cfd3[str(e)]=np.sum(np.sum(cfd_mesh['U3']**2,axis=1)*vol_cfd)*1/2*1060*1000 #mJ

data={}
data['tke_cfd0']=tke_cfd0
data['tke_cfd1']=tke_cfd1
data['tke_cfd2']=tke_cfd2
data['tke_cfd3']=tke_cfd3
data['ke_cfd0']=ke_cfd0
data['ke_cfd1']=ke_cfd1
data['ke_cfd2']=ke_cfd2
data['ke_cfd3']=ke_cfd3
np.save('data_cfd.npy',data)
