"""
Signal based 4D flow MRI synthesizer
Extract turbulent kinetic energy data from synthetic MRI images.

Copyright © 2022 Pietro Dirix, Stefano Buoso, Eva Peper and Sebastian Kozerke
Licensed under the MIT License (see LICENSE for details)
Date: 06-2022
Author: Pietro Dirix
"""

import numpy as np
import os, glob
import pyvista as pv

mri_files=glob.glob(os.path.join(os.getcwd(),'../*.vti'))

#Extract data on the whole geometry
tke_mri0={}
tke_mri1={}
tke_mri2={}
tke_mri3={}

ke_mri0={}
ke_mri1={}
ke_mri2={}
ke_mri3={}

for e,mri in enumerate(mri_files):
    print('Extracting mri data n',e)
    voxel_s=int(mri.split('voxel')[-1].split('_')[0])
    dt_s=int(mri.split('dt')[-1].split('_')[0])

    mri_mesh=pv.read(mri)
    vol_mri=mri_mesh.compute_cell_sizes(length=False, area=False, volume=True)['Volume']
    print('Case '+str(voxel_s)+'_'+str(dt_s))
    tke_mri0[str(voxel_s)+'_'+str(dt_s)]=np.sum(np.sum(mri_mesh['RST_recon'][:,[0,1,2]],axis=1)*vol_mri)*1/2*1000 #mJ
    ke_mri0[str(voxel_s)+'_'+str(dt_s)]=np.sum(np.sum(mri_mesh['Vcart']**2,axis=1)*vol_mri)*1/2*1060*1000 #mJ

data={}
data['tke_mri']=tke_mri0
data['ke_mri']=ke_mri0
np.save('data_mri.npy',data)

#Extract data on a target region of interest
tke_mri0={}
tke_mri1={}
tke_mri2={}
tke_mri3={}

ke_mri0={}
ke_mri1={}
ke_mri2={}
ke_mri3={}

for e,mri in enumerate(mri_files):
    print('Extracting mri data n',e)
    voxel_s=int(mri.split('voxel')[-1].split('_')[0])
    dt_s=int(mri.split('dt')[-1].split('_')[0])

    mri_mesh=pv.read(mri)
    mri_mesh=mri_mesh.clip('x',origin=[0.2,0,0],invert=True)
    mri_mesh=mri_mesh.clip('x',origin=[0.09,0,0],invert=False)
    try: vol_mri2
    except: vol_mri2=mri_mesh.compute_cell_sizes(length=False, area=False, volume=True)['Volume']
    print('Case '+str(voxel_s)+'_'+str(dt_s))
    tke_mri0[str(voxel_s)+'_'+str(dt_s)]=np.sum(np.sum(mri_mesh['RST_recon'][:,[0,1,2]],axis=1)*vol_mri2)*1/2*1000 #mJ
    ke_mri0[str(voxel_s)+'_'+str(dt_s)]=np.sum(np.sum(mri_mesh['Vcart']**2,axis=1)*vol_mri2)*1/2*1060*1000 #mJ

data={}
data['tke_mri']=tke_mri0
data['ke_mri']=ke_mri0
np.save('data_mri_roi.npy', data)
